<?php
class Vivienda extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }
  public function getViviendas()
  {
    $this->db->order_by("id", "asc");
    $viviendas = $this->db->get('datos-vivienda');
    if ($viviendas->num_rows() > 0) {
      return $viviendas->result();
    } else {
      return false;
    }
  }
  public function guardarVivienda($datos){
    $this->db->insert('datos-vivienda', $datos);
  }
}
