<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model("Vivienda");
	}

	public function index()
	{
		$data["listaViviendas"] = $this->Vivienda->getViviendas();
		$this->load->view('welcome_message', $data);
	}

	public function guardarVivienda()
	{
		$datos = array(
			"cedula" => $this->input->post("cedula"),
			"apellidos" => $this->input->post("apellidos"),
			"nombres" => $this->input->post("nombres"),
			"celular" => $this->input->post("celular"),
			"correo" => $this->input->post("correo"),
			"paths" => $this->input->post("paths")
		);
		$this->Vivienda->guardarVivienda($datos);
		redirect(base_url());
	}
}
