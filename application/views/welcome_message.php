<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFdikaj-qpZIQZN5Y48Z_5oFSNMKlTlnA&libraries=places&callback=initMap"></script>
  <script>
    function initMap() {
      //Iniciar el mapa y centrarlo en la UTC
      let mapa = new google.maps.Map(document.getElementById("mapDrag"), {
        zoom: 15,
        center: new google.maps.LatLng(-0.9182051699633873, -78.63237646153095),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      });
      //Iniciar el mapa que tendra los paths de la BD
      let mapa2 = new google.maps.Map(document.getElementById("mapa2"), {
        zoom: 14.8,
        center: new google.maps.LatLng(-0.9182051699633873, -78.63237646153095),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      });
      //Array que contendra todos los paths de la BD
      let pathsArray = []
      <?php foreach ($listaViviendas as $vivienda) : ?>
        //Llenando el array con los paths de la BD
        pathsArray.push(<?php echo $vivienda->paths ?>);
      <?php endforeach ?>
      const poligonosMapaDibujar = new google.maps.Polygon({
        paths: pathsArray,
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
      });

      poligonosMapaDibujar.setMap(mapa2);

      //Crear los marcadores
      let marcador1 = new google.maps.Marker({
        position: new google.maps.LatLng(-0.9165072757445483, -78.6346024430943),
        map: mapa,
        title: "Marcador 1",
        draggable: true,
      });
      let marcador2 = new google.maps.Marker({
        position: new google.maps.LatLng(-0.9193797845808189, -78.63280951939234),
        map: mapa,
        title: "Marcador 2",
        draggable: true,
      });
      let marcador3 = new google.maps.Marker({
        position: new google.maps.LatLng(-0.9193797823289963, -78.63227929923413),
        map: mapa,
        title: "Marcador 3",
        draggable: true,
      });
      let marcador4 = new google.maps.Marker({
        position: new google.maps.LatLng(-0.916392360707634, -78.63282732670685),
        map: mapa,
        title: "Marcador 4",
        draggable: true,
      });
      //Crear el polígono
      let paths = [
        marcador1.getPosition(),
        marcador2.getPosition(),
        marcador3.getPosition(),
        marcador4.getPosition(),
      ];
      //Setear pats en los inputs
      function setearPath() {
        document.getElementById("paths").value = JSON.stringify(paths);
      }
      //Actualizar el polígono
      const actualizarPaths = () => {
        paths = [
          marcador1.getPosition(),
          marcador2.getPosition(),
          marcador3.getPosition(),
          marcador4.getPosition(),
        ];
        poligonoMapa.setPaths(paths);
        setearPath();
      }

      marcador1.addListener("dragend", function(event) {
        actualizarPaths();
      });
      marcador2.addListener("dragend", function(event) {
        actualizarPaths();
      });
      marcador3.addListener("dragend", function(event) {
        actualizarPaths();
      });
      marcador4.addListener("dragend", function(event) {
        actualizarPaths();
      });

      const poligonoMapa = new google.maps.Polygon({
        paths,
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
      });

      poligonoMapa.setMap(mapa);
    }
  </script>

  <title>Registro de Propiedad UTC</title>

</head>


<body class="bg-slate-900">
  <div class="flex">

    <div class="w-1/2 bg-gray-100">
      <h1 class="text-center text-3xl text-gray-900">Registro de Propiedades</h1>
      <hr>
      <form action="<?php echo site_url('Welcome/guardarVivienda') ?>" method="POST" class="flex flex-col justify-center items-center my-3 gap-2 p-5">
        <input name="cedula" type="text" class="text-black w-1/2 border border-gray-300 focus:outline-none focus:border-yellow-500 border-solid rounded-md p-2" placeholder="Ingrese la cédula" required>
        <input name="apellidos" type="text" class="text-black w-1/2 border border-gray-300 focus:outline-none focus:border-yellow-500 border-solid rounded-md p-2" placeholder="Ingrese los apellidos" required>
        <input name="nombres" type="text" class="text-black w-1/2 border border-gray-300 focus:outline-none focus:border-yellow-500 border-solid rounded-md p-2" placeholder="Ingrese los nombres" required>
        <div class="flex w-1/2 gap-2">
          <input name="celular" type="number" class="text-black w-1/2 border border-gray-300 focus:outline-none focus:border-yellow-500 border-solid rounded-md p-2" placeholder="Número celular" required>
          <input name="correo" type="email" class="text-black w-1/2 border border-gray-300 focus:outline-none focus:border-yellow-500 border-solid rounded-md p-2" placeholder="Correo electrónico" required>
        </div>
        <input name="paths" type="text" id="paths" class="hidden text-black w-1/2 border border-gray-300 focus:outline-none focus:border-yellow-500 border-solid rounded-md p-2" placeholder="paths">
        <div id="mapDrag" class="border border-black border-solid w-2/3 h-72"></div>
        <button type="submit" class="bg-emerald-400 px-6 py-3 rounded-md hover:bg-emerald-500">Guardar</button>
      </form>
    </div>

    <div class="w-1/2 bg-gray-100">
      <h1 class="text-center text-3xl text-gray-900">Listado</h1>
      <hr>
      <?php if ($listaViviendas) : ?>
        <table class="table-auto w-full">
          <thead>
            <tr>
              <th class="border border-black border-solid">Cédula</th>
              <th class="border border-black border-solid">Apellidos</th>
              <th class="border border-black border-solid">Nombres</th>
              <th class="border border-black border-solid">Celular</th>
              <th class="border border-black border-solid">Correo</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($listaViviendas as $vivienda) : ?>
              <tr>
                <td class="border border-black border-solid"><?php echo $vivienda->cedula ?></td>
                <td class="border border-black border-solid"><?php echo $vivienda->apellidos ?></td>
                <td class="border border-black border-solid"><?php echo $vivienda->nombres ?></td>
                <td class="border border-black border-solid"><?php echo $vivienda->celular ?></td>
                <td class="border border-black border-solid"><?php echo $vivienda->correo ?></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php else : ?>
        <h1 class="text-center text-3xl text-gray-900">No hay viviendas registradas</h1>
      <?php endif; ?>
      <div id="mapa2" class="w-full h-[70vh] border-2 border-solid border-black my-5">
      </div>
    </div>

  </div>

</body>


</html>